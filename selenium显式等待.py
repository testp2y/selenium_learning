from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

browser=webdriver.Chrome()
browser.get('https://www.taobao.com/')
#指定最长等待时间
wait=WebDriverWait(browser,10)
#节点的定位元祖 也就是ID为q的节点搜索框
input=wait.until(EC.presence_of_all_elements_located((By.ID,'q')))
#查找按钮
buton=wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,'.btn-search')))
print(input,buton)
